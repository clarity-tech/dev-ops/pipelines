# pipelines

Ready made configurations for standard Gitlab CI/CD pipeline templates

with best practises and zero downtime configs

set these in gitlab CI/CD variables to reflect the URLS and environment variables
```bash
ENV_STAGING
STAGING_URL
```

```bash
PROD_URL
ENV_PROD
```

## Getting started


files are prefixed with

`.defination.gitlab-ci.yml` for only the definations excluding the actual jobs


`.jobs.gitlab-ci.yml` for only the jobs

builds folder contains jobs and definations of different type of builds

contains builds related to laravel pipelines
`builds/laravel`



`deployments/envoy`
contains jobs and build definations related to envoy deployments


`deployments/laravel`
contains jobs related to laravel docs deployments


`prepare-ssh`

contains definations related to staging and prod enviornment setup


`security`

contains secuirty sast or vulnerability scans


`tests/laravel`
contains jobs and build definations related to laravel tests

## Usage
Sample gitlab ci yml to be used in a project

```yml
# overriding the variables as per the project needs
variables:
  MYSQL_DATABASE: your-own-db-name
  TARGET_HOST: server-ip-or-alias
  SSH_KEY_ID: $DEPLOYER_SSH_KEY_ID
  TARGET_DIR: /home/deployer/$DEPLOY_PROJECT_DIR/releases

include:
  # - local: .gitlab/ci/**.yml
  - project: 'clarity-tech/dev-ops/pipelines'
    file:
      - 'Auto-DevOps-laravel-envoy-php8.1.gitlab-ci.yml'
      - 'templates/deployments/laravel/scribe-deploy-docs.jobs.gitlab-ci.yml'

# overriding the tags as per the project needs

build backend:
  tags:
    - arm
    - clarity
    - docker

build backend test:
  tags:
    - arm
    - clarity
    - docker

unit tests:
  tags:
    - arm
    - clarity

.deploy_setup_staging:
  tags:
    - clarity
    - deploy
    - production
    - any-other-tags-that-you-want-to-control-the-behavior

.deploy_setup_prod:
  tags:
    - clarity
    - deploy
    - production
    - your-custom-tags

```

## Support
Need any other help, send email to support@claritytech.io

## Roadmap
Give more examples and update readme for usages

## Contributing
Any new idea or bugs?, Please send a MR to the main branch


## License
GNU GPL

## Project status
Actively maintained
